# generator-hugo [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> Generate websites with Hugo and deploy with AWS.

## Installation

First, install [Yeoman](http://yeoman.io) and generator-hugo using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-hugo
```

Then generate your new project:

```bash
yo hugo
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

Apache-2.0 © [mega-mac-slice](https://mega-mac.com)


[npm-image]: https://badge.fury.io/js/generator-hugo.svg
[npm-url]: https://npmjs.org/package/generator-hugo
[travis-image]: https://travis-ci.org/mega-mac-slice/generator-hugo.svg?branch=master
[travis-url]: https://travis-ci.org/mega-mac-slice/generator-hugo
[daviddm-image]: https://david-dm.org/mega-mac-slice/generator-hugo.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/mega-mac-slice/generator-hugo
