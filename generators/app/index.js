'use strict';
const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');

module.exports = class extends Generator {
  prompting() {
    // Have Yeoman greet the user.
    this.log(
      yosay(`Welcome to the scrumtrulescent ${chalk.red('generator-hugo')} generator!`)
    );

    const prompts = [
      {
        type: 'input',
        name: 's3Bucket',
        message: 'What is the S3-bucket?',
        default: 'example.org'
      },
      {
        type: 'input',
        name: 's3Region',
        message: 'What is the S3-region?',
        default: 'us-west-2'
      }
    ];

    return this.prompt(prompts).then(props => {
      this.props = props;
    });
  }

  runHugoNewSite() {
    return this.spawnCommandSync('hugo', ['new', 'site', '.']);
  }

  addRootTemplates() {
    return this.fs.copyTpl(
      this.templatePath('*'),
      this.destinationRoot(),
      {s3Bucket: this.props.s3Bucket, s3Region: this.props.s3Region}
    );
  }
  addStaticTemplates() {
    return this.fs.copyTpl(
      this.templatePath('static/*'),
      this.destinationPath('static'),
      {s3Bucket: this.props.s3Bucket, s3Region: this.props.s3Region}
    );
  }

  gitDependencies() {
    this.spawnCommandSync('git', ['init']);
    this.spawnCommandSync('git', ['add', '.']);
    this.spawnCommandSync('git', ['commit', '-m', 'init']);
    this.spawnCommandSync('git', ['subtree', 'add', '--prefix', '.core-devops', 'git@gitlab.com:mega-mac-slice/core-devops.git', 'master', '--squash']);
  }

  writing() {
  }

  install() {
  }
};
