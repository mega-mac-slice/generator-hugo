# <%= s3Bucket %>

## Development
```bash
make dev
```

## Deployment
```bash
make dist && make deploy
```

## References
- [Hugo](https://gohugo.io)
- [Hugo And AWS](https://mtyurt.net/post/serverless-blog-with-hugo-and-aws.html)
- [Host On S3](https://medium.freecodecamp.org/how-to-host-a-website-on-s3-without-getting-lost-in-the-sea-e2b82aa6cd38)
